﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RPG.Core;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction
{
      [SerializeField] float maxSpeed = 6f;
    NavMeshAgent navMeshAgent;
    Health health;
    
    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>(); // 简化
        health = GetComponent<Health>();
    }
    // Update is called once per frame
    void Update()
    {
        
        navMeshAgent.enabled = !health.IsDead(); //尸体不会有碰撞体积

        UpdateAnimator();  
        
    }

      public void StartMoveAction(Vector3 destination, float speedFraction)
    {
         GetComponent<ActionScheduler>().StartAction(this);
          MoveTo(destination, speedFraction);
        
    }
 

    public void MoveTo(Vector3 destination, float speedFraction)
    {
        navMeshAgent.destination = destination;//指哪走哪
        navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);//Y移速波动
        navMeshAgent.isStopped = false;
    }

    public void Cancel()
    {
        navMeshAgent.isStopped = true;

    }




    private void UpdateAnimator()
    {
        Vector3 velocity = navMeshAgent.velocity;//速度global to local
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);
        float speed = localVelocity.z;
        GetComponent<Animator>().SetFloat("fowardSpeed",speed);

    }

}
}