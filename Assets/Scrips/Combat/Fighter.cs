using UnityEngine;
using RPG.Movement;
using RPG.Core;
namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] float weaponRange = 2f;
        [SerializeField] float timeBetweenAttacks = 1f;
        [SerializeField] float weaponDamage = 5f;
        Health target;
        float timeSinceLastAttack = 0;
        private void Update()
        {
            timeSinceLastAttack += Time.deltaTime;
            if (target == null) return;
            if (target.IsDead()) return; //不鞭尸
            if (!GetIsInRange())
            {
             GetComponent<Mover>().MoveTo(target.transform.position, 1f);//移动到攻击范围内才攻击
            }
            else
            {
                GetComponent<Mover>().Cancel();//到了范围内才开始攻击指令
                AttackBehaviour();
            }
        }
        private void AttackBehaviour()
        {
            transform.LookAt(target.transform); //攻击需要面朝目标
            if (timeSinceLastAttack > timeBetweenAttacks) 
            {
                // 判断攻击间隔，与上次攻击时间大于攻击间隔时间 才触发下一次攻击

                GetComponent<Animator>().ResetTrigger("stopAttack");//避免攻击动画被打断
                GetComponent<Animator>().SetTrigger("attack unarmed");

                timeSinceLastAttack = Mathf.Infinity; //立即攻击
            }
        }
        // Animation Event
        void Hit()
        {
            if (target == null) return;// 避免有几率造成的null reference
            target.TakeDamage(weaponDamage);//伤害为武器伤害
        }
        private bool GetIsInRange() 
        {
            return Vector3.Distance(transform.position, target.transform.position) < weaponRange; //判断武器范围和攻击范围
        }

        public bool CanAttack(GameObject combatTarget) //判断目标是否死亡
        {
            if (combatTarget == null) { return false; }
            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && !targetToTest.IsDead();
        }

        public void Attack(GameObject combatTarget)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            target = combatTarget.GetComponent<Health>();
        }
        public void Cancel()//可以走A 移动打断攻击
        {
            GetComponent<Animator>().SetTrigger("stopAttack");
            target = null;
        }
    }
}