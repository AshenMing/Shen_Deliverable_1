using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour //AI寻路
    {
        const float waypointGizmoRadius = 0.3f;

        private void OnDrawGizmos() {
            for (int i = 0; i < transform.childCount; i++) //有几个point标几个
            {
               int j = GetNextIndex(i);//循环+1
                Gizmos.DrawSphere(GetWaypoint(i), waypointGizmoRadius);//球形gizmos显示
                Gizmos.DrawLine(GetWaypoint(i), GetWaypoint(j));//画线
            }
        }
          public int GetNextIndex(int i)
        {
            if (i + 1 == transform.childCount)
            {
                return 0;
            }
            return i + 1;
        }

        public Vector3 GetWaypoint(int i)
        {
            return transform.GetChild(i).position;
        }
    
    
    }
}