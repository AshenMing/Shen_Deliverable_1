﻿using System.Collections;
﻿using System;
using System.Collections.Generic;
using RPG.Combat;
using UnityEngine;
using RPG.Core;
using RPG.Movement;

namespace RPG.Control
	{public class AIController : MonoBehaviour
    {
        [SerializeField] float chaseDistance = 5f; //追踪距离
        [SerializeField] float suspicionTime = 3f;
        [SerializeField] PatrolPath patrolPath;
         [SerializeField] float waypointTolerance = 1f; //
         [SerializeField] float waypointDwellTime = 3f;
    
        [SerializeField] float patrolSpeedFraction = 0.2f;

        Fighter fighter;
        GameObject player;
        Health health;
         Mover mover;

       Vector3 guardPosition;
        float timeSinceLastSawPlayer = Mathf.Infinity;
        float timeSinceArrivedAtWaypoint = Mathf.Infinity;
         int currentWaypointIndex = 0;


        private void Start() {
            fighter = GetComponent<Fighter>();
            player = GameObject.FindWithTag("Player");
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();

            guardPosition = transform.position;
        }

        private void Update()
        {
            if (health.IsDead()) return; //死亡就停止一切

            if (InAttackRangeOfPlayer() && fighter.CanAttack(player)) //攻击范围内且角色没死
            {
                 timeSinceLastSawPlayer = 0;
                AttackBehaviour();
            }
            else if (timeSinceLastSawPlayer < suspicionTime)//警戒怀疑计算
            {
                 SuspicionBehaviour();//警戒/怀疑状态
                
            }
            else
            {
                PatrolBehaviour(); //超出追踪范围就取消追踪并且返回guard点
            
            }
              UpdateTimers();
        }

        private void UpdateTimers()
        {
            timeSinceLastSawPlayer += Time.deltaTime;//警戒时间相关计算
            timeSinceArrivedAtWaypoint += Time.deltaTime;
            
        }
        private void PatrolBehaviour()//守卫状态/寻路
        {
                 Vector3 nextPosition = guardPosition;

            if (patrolPath != null)
            {
                if (AtWaypoint())//判断point距离 近的开始
                {
                    timeSinceArrivedAtWaypoint = 0;
                    CycleWaypoint();
                }
                nextPosition = GetCurrentWaypoint();//下一个点
            }

               if (timeSinceArrivedAtWaypoint > waypointDwellTime)//每个点停一下
               
            {
                 mover.StartMoveAction(nextPosition, patrolSpeedFraction);
            }
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void CycleWaypoint()
        {
            currentWaypointIndex = patrolPath.GetNextIndex(currentWaypointIndex);//寻路
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypoint(currentWaypointIndex);
        }

        

        private void SuspicionBehaviour()//警戒/
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour()//攻击
        {
            timeSinceLastSawPlayer = 0;
            fighter.Attack(player);
        }

        private bool InAttackRangeOfPlayer()//攻击距离是否小于与玩家距离
        {
            float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
            return distanceToPlayer < chaseDistance;
        }
     private void OnDrawGizmosSelected() //unity自带 ，看chase距离
       {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }
    }

 
}