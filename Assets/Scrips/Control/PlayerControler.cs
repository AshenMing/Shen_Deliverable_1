using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Core;

namespace RPG.Control
{

public class PlayerControler : MonoBehaviour
{   
    Health health;
  private void Start() 
  {
            health = GetComponent<Health>();
  }
   
    
private void Update()

{   if (health.IsDead()) return;
    if(InteractWithCombat()) return;//避免每次同时触发攻击与移动
     if(InteractWithMovement()) return;
     print("NOthing to do.");//无有效指令
}

private bool InteractWithMovement()
{
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit); //打出hitray
            if(hasHit)
            {

            if ( Input.GetMouseButton(0))
            {
            GetComponent<Mover>().StartMoveAction(hit.point, 1f);//指哪去哪
            }
            return true;
            } 
        return false;
}

private bool InteractWithCombat()//bool返还
{
    RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
    foreach(RaycastHit hit in hits)
    {
       CombatTarget target = hit.transform.GetComponent<CombatTarget>();//如果无target变无攻击
       
       if(target == null) continue;
       GameObject targetGameObject = target.gameObject;//转化target为gameobject
       
       if(!GetComponent<Fighter>().CanAttack(target.gameObject))
       {
        continue;
       }
       if(Input.GetMouseButton(0)) 
       {
        GetComponent<Fighter>().Attack(target.gameObject);//Fighter交互 攻击target
       }
       return true;


    }
    return false;//没发现target交互;

}
    private static Ray GetMouseRay()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition); //得到鼠标所指
    }
}


}