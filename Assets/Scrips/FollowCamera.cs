using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] float rotationSpeed = 2f; // Adjust the rotation speed

        private float pitchAngle = 0f;    // Store the current pitch angle for clamping

        void LateUpdate()
        {
            transform.position = target.position;
            RotateView();
        }

        void RotateView()
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            
            // Apply rotation while preserving the Y position
            transform.eulerAngles = new Vector3(pitchAngle, transform.eulerAngles.y + horizontalInput * rotationSpeed, 0);
        }
    }
}
