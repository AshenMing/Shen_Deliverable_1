using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;
using UnityEngine.UI; // For UI elements like Image

public class IntroSequenceController : MonoBehaviour
{
    public PlayableDirector introDirector; // Assign this in the Inspector
    public CinemachineVirtualCamera followCamera; // Assign your follow camera in the Inspector
    public GameObject endMask; // Assign the EndMask GameObject in the Inspector
    public GameObject beginText;

    private void Start()
    {
        // Ensure the follow camera and end mask are initially disabled
        followCamera.gameObject.SetActive(false);
        endMask.SetActive(true); // Assuming you want the mask active at start
        beginText.SetActive(false); 

        // Subscribe to the event that is called when the timeline finishes playing
        introDirector.stopped += OnIntroSequenceFinish;
        
        


    }

    private void OnIntroSequenceFinish(PlayableDirector director)
    {
        if (director == introDirector)
        {
            // Enable the follow camera
            followCamera.gameObject.SetActive(true);

            // Disable the end mask
            endMask.SetActive(false);
beginText.SetActive(true); 
            // Optional: If you want to disable the game object that contains the director,
            // so it doesn't play again if the scene is reset or something else happens.
            introDirector.gameObject.SetActive(false);
        }
    }
}
