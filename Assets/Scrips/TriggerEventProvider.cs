using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventProvider : MonoBehaviour
{
    [SerializeField] private string tagString;
    [SerializeField] UnityEvent onTriggerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals(tagString))
        {
            onTriggerEnter?.Invoke();
        }
    }
}
