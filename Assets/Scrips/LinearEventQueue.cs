using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LinearEventQueue : MonoBehaviour
{
    [SerializeField] private List<UnityEvent> events;

    private Queue<UnityEvent> eventQueue = new Queue<UnityEvent>();

    private void Awake()
    {
        foreach(var evt in events)
        {
            eventQueue.Enqueue(evt);
        }
        DequeueEvent();
    }

    public void DequeueEvent()
    {
        if(eventQueue.Count > 0)
        {
            UnityEvent nextEvent = eventQueue.Dequeue();
            nextEvent?.Invoke();
        }
    }
}

