using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public float rotationSpeed = 20.0f;  // Speed of rotation

    private float rotationY = 0.0f;

    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); // A and D keys

        // Calculate rotation based on input
        rotationY += horizontalInput * rotationSpeed * Time.deltaTime;

        // Apply rotation to camera
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, rotationY, 0.0f);
    }
}
