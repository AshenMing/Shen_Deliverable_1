using UnityEngine;
using RPG.Core;

namespace RPG.Core
{
    public class ActionScheduler : MonoBehaviour
    {
        IAction currentAction;

        public void StartAction(IAction action)
        {
             if ( currentAction== action) return;

             if ( currentAction != null)
        {
            currentAction.Cancel();//不同时进行多种动作
        }
        currentAction = action;

        }

        public void CancelCurrentAction()
        {
            StartAction(null);
        }
    }
}