
using UnityEngine;
using RPG.Core;

namespace RPG.Core
{
public class Health : MonoBehaviour
{
    [SerializeField] float healthPoints = 100f;
   
   bool isDead = false;

   public bool IsDead()
   {
        return isDead;
   }
    public void TakeDamage(float damage) //method获得weapon武器
    {
        healthPoints = Mathf.Max(healthPoints - damage, 0);
        if(healthPoints ==0)
        {
            Die();
        }
    } 

    private void Die()
    {
        if(isDead) return;

        isDead = true;
        GetComponent<Animator>().SetTrigger("die");//触发死亡
        GetComponent<ActionScheduler>().CancelCurrentAction();
    }
}


}
